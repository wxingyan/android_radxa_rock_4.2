/******************************************************************************
 *
 *  Copyright (C) 2012 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/
package com.broadcom.app.leexplorer.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.broadcom.app.leexplorer.R;
import com.broadcom.app.leexplorer.ValueFragment;
import com.broadcom.bt.gatt.BluetoothGattCharacteristic;

public class HeartRateControlFragment extends ValueFragment {
    private static final int RESET_ENERGY_EXPENDED = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_heartrate_control, container, false);
    }

    @Override
    public void setValue(byte[] value) {
        // Write-only characteristic
    }

    @Override
    public boolean assignValue(BluetoothGattCharacteristic characteristic) {
        characteristic.setValue(RESET_ENERGY_EXPENDED, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
        return true;
    }
}
