/******************************************************************************
 *
 *  Copyright (C) 2012 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/
package com.broadcom.app.leexplorer;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.broadcom.bt.gatt.BluetoothGatt;
import com.broadcom.bt.gatt.BluetoothGattAdapter;
import com.broadcom.bt.gatt.BluetoothGattCallback;
import com.broadcom.bt.gatt.BluetoothGattCharacteristic;
import com.broadcom.bt.gatt.BluetoothGattDescriptor;
import com.broadcom.bt.gatt.BluetoothGattService;

public class GattAppService extends Service {

    private static final String TAG = MainActivity.TAG;

    /** Intents to indicate GATT state */
    public static final String GATT_DEVICE_FOUND = "com.broadcom.gatt.device_found";
    public static final String GATT_DEVICE_LOST = "com.broadcom.gatt.device_lost";
    public static final String GATT_CONNECTION_STATE = "com.broadcom.gatt.connection_state";
    public static final String GATT_SERVICES_REFRESHED = "com.broadcom.gatt.refreshed";
    public static final String GATT_CHARACTERISTIC_READ = "com.broadcom.gatt.read";

    /** Intent extras */
    public static final String EXTRA_DEVICE = "DEVICE";
    public static final String EXTRA_RSSI = "RSSI";
    public static final String EXTRA_SOURCE = "SOURCE";
    public static final String EXTRA_ADDR = "ADDRESS";
    public static final String EXTRA_CONNECTED = "CONNECTED";
    public static final String EXTRA_STATUS = "STATUS";
    public static final String EXTRA_UUID = "UUID";
    public static final String EXTRA_VALUE = "VALUE";

    /** Source of device entries in the device list */
    public static final int DEVICE_SOURCE_SCAN = 0;
    public static final int DEVICE_SOURCE_BONDED = 1;
    public static final int DEVICE_SOURCE_CONNECTED = 2;

    /** Descriptor used to enable/disable notifications/indications */
    private static final UUID CLIENT_CONFIG_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    private final IBinder binder = new LocalBinder();
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothGatt mBluetoothGatt = null;
    List <String> mReconnectList = new ArrayList<String>();
    private boolean mScanning = false;

    private BluetoothGattCallback mGattCallbacks = new BluetoothGattCallback() {
        @Override
        public void onAppRegistered(int status) {
            Log.d(TAG, "onAppRegistered() - status=" + status);
            if (mScanning) mBluetoothGatt.startScan();
        }

        @Override
        public void onScanResult(BluetoothDevice device, int rssi, byte[] scanRecord) {
            Log.d(TAG, "onScanResult() - device=" + device + ", rssi=" + rssi);
            sendDeviceFoundIntent(device, rssi, DEVICE_SOURCE_SCAN);
        }

        @Override
        public void onConnectionStateChange(BluetoothDevice device, int status, int newState) {
            Log.d(TAG, "onConnectionStateChange() - device=" + device + ", state=" + newState
                    + ", reconnect=" + getReconnect(device.getAddress()));

            Intent intent = new Intent(GATT_CONNECTION_STATE);
            intent.putExtra(EXTRA_ADDR, device.getAddress());
            intent.putExtra(EXTRA_CONNECTED, newState == BluetoothProfile.STATE_CONNECTED);
            intent.putExtra(EXTRA_STATUS, status);
            sendBroadcast(intent);

            if (newState == BluetoothProfile.STATE_CONNECTED && mBluetoothGatt != null) {
                sendDeviceFoundIntent(device, 255, DEVICE_SOURCE_CONNECTED);
                mBluetoothGatt.discoverServices(device);
            }

            if (newState == BluetoothProfile.STATE_DISCONNECTED && mBluetoothGatt != null) {
                sendDeviceLostIntent(device);
                if (getReconnect(device.getAddress())) {
                    mBluetoothGatt.connect(device, true);
                } else {
                    mBluetoothGatt.cancelConnection(device);
                }
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothDevice device, int status) {
            Log.d(TAG, "onServicesDiscovered() - device=" + device + ", status=" + status);
            Intent intent = new Intent(GATT_SERVICES_REFRESHED);
            intent.putExtra(EXTRA_ADDR, device.getAddress());
            intent.putExtra(EXTRA_STATUS, status);
            sendBroadcast(intent);
        }

        @Override
        public void onCharacteristicRead(BluetoothGattCharacteristic characteristic, int status) {
            Log.d(TAG, "onCharacteristicRead() - characteristic=" + characteristic.getUuid()+ ", status=" + status);
            Intent intent = new Intent(GATT_CHARACTERISTIC_READ);
            intent.putExtra(EXTRA_UUID, characteristic.getUuid().toString());
            intent.putExtra(EXTRA_STATUS, status);
            intent.putExtra(EXTRA_VALUE, characteristic.getValue());
            sendBroadcast(intent);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGattCharacteristic characteristic) {
            onCharacteristicRead(characteristic, 0);
        }
    };

    private BluetoothProfile.ServiceListener mProfileServiceListener =
            new BluetoothProfile.ServiceListener() {
        @Override
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            Log.d(TAG, "Gatt proxy service connected");
            mBluetoothGatt = (BluetoothGatt) proxy;
            mBluetoothGatt.registerApp(mGattCallbacks);
        }

        @Override
        public void onServiceDisconnected(int profile) {
            Log.d(TAG, "Gatt proxy service disconnected");
            mBluetoothGatt = null;
        }
    };

    public class LocalBinder extends Binder {
        GattAppService getService() {
            return GattAppService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Bound to app service");
        return binder;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Destroying app service");
        mReconnectList.clear();
        if (mBluetoothAdapter != null && mBluetoothGatt != null) {
            BluetoothGattAdapter.closeProfileProxy(BluetoothGattAdapter.GATT, mBluetoothGatt);
        }

        super.onDestroy();
    }

    public boolean init() {
        if (mBluetoothAdapter == null) {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) return false;
        }

        if (mBluetoothGatt == null) {
            return BluetoothGattAdapter.getProfileProxy(this, mProfileServiceListener, BluetoothGattAdapter.GATT);
        }

        return true;
    }

    public void scan(boolean start) {
        mScanning = start;
        if (mBluetoothGatt == null) return;

        if (start) {
            addBondedDevices();
            addConnectedDevices();

            mBluetoothGatt.startScan();
        } else {
            mBluetoothGatt.stopScan();
        }
    }

    public boolean isScanning() {
        return mScanning;
    }

    public void setReconnect(String address, boolean reconnect) {
        if (reconnect) {
            mReconnectList.add(address);
        } else {
            mReconnectList.remove(address);
        }
    }

    public boolean getReconnect(String address) {
        return mReconnectList.contains(address);
    }

    public void connect(String address) {
        if (mBluetoothGatt == null) return;

        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) return;

        mBluetoothGatt.connect(device, false);
    }

    public void disconnect(String address) {
        Log.d(TAG, "disconnect() - address=" + address);
        if (mBluetoothGatt == null) return;

        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) return;

        mBluetoothGatt.cancelConnection(device);
    }

    public void discover(String address) {
        if (mBluetoothGatt == null) return;

        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) return;

        mBluetoothGatt.refresh(device);
        mBluetoothGatt.discoverServices(device);
    }

    public List<BluetoothGattService> getServices(String address) {
        if (mBluetoothGatt == null) return null;
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) return null;
        return mBluetoothGatt.getServices(device);
    }

    public BluetoothGattCharacteristic getCharacteristic(String address, UUID serviceUuid, UUID charUuid) {
        if (mBluetoothGatt == null) return null;
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        if (device == null) return null;
        BluetoothGattService service = mBluetoothGatt.getService(device, serviceUuid);
        if (service == null) return null;
        return service.getCharacteristic(charUuid);
    }

    public boolean readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothGatt == null) return false;
        return mBluetoothGatt.readCharacteristic(characteristic);
    }

    public boolean writeCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothGatt == null) return false;
        return mBluetoothGatt.writeCharacteristic(characteristic);
    }

    public boolean enableNotification(boolean enable, BluetoothGattCharacteristic characteristic) {
        if (mBluetoothGatt == null) return false;
        if (!mBluetoothGatt.setCharacteristicNotification(characteristic, enable)) return false;

        BluetoothGattDescriptor clientConfig = characteristic.getDescriptor(CLIENT_CONFIG_UUID);
        if (clientConfig == null) return false;

        if (enable) {
            clientConfig.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        } else {
            clientConfig.setValue( new byte[] {0x00, 0x00});
        }
        return mBluetoothGatt.writeDescriptor(clientConfig);
    }

    public boolean enableIndication(boolean enable, BluetoothGattCharacteristic characteristic) {
        if (mBluetoothGatt == null) return false;
        if (!mBluetoothGatt.setCharacteristicNotification(characteristic, enable)) return false;

        BluetoothGattDescriptor clientConfig = characteristic.getDescriptor(CLIENT_CONFIG_UUID);
        if (clientConfig == null) return false;

        if (enable) {
            clientConfig.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
        } else {
            clientConfig.setValue( new byte[] {0x00, 0x00});
        }
        return mBluetoothGatt.writeDescriptor(clientConfig);
    }

    private void sendDeviceFoundIntent(BluetoothDevice device, int rssi, int source) {
        Intent intent = new Intent(GATT_DEVICE_FOUND);
        intent.putExtra(EXTRA_DEVICE, device);
        intent.putExtra(EXTRA_RSSI, rssi);
        intent.putExtra(EXTRA_SOURCE, source);
        sendBroadcast(intent);
    }

    private void sendDeviceLostIntent(BluetoothDevice device) {
        Intent intent = new Intent(GATT_DEVICE_FOUND);
        intent.putExtra(EXTRA_DEVICE, device);
        sendBroadcast(intent);
    }

    private void addBondedDevices() {
        if(mBluetoothAdapter == null) return;
        Set<BluetoothDevice> bondedDevices = mBluetoothAdapter.getBondedDevices();
        for (BluetoothDevice bondedDevice : bondedDevices) {
            sendDeviceFoundIntent(bondedDevice, 0, DEVICE_SOURCE_BONDED);
        }
    }

    private void addConnectedDevices() {
        if(mBluetoothGatt == null) return;
        List<BluetoothDevice> connectedDevices = mBluetoothGatt.getConnectedDevices();
        for (BluetoothDevice connectedDevice : connectedDevices) {
            sendDeviceFoundIntent(connectedDevice, 0, DEVICE_SOURCE_CONNECTED);
        }
    }
}
