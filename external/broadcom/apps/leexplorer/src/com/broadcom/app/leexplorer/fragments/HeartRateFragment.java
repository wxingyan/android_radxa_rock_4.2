/******************************************************************************
 *
 *  Copyright (C) 2012 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/
package com.broadcom.app.leexplorer.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.broadcom.app.leexplorer.DataUtils;
import com.broadcom.app.leexplorer.R;
import com.broadcom.app.leexplorer.ValueFragment;

public class HeartRateFragment extends ValueFragment {
    private static final int FLAG_FORMAT_UINT16 = 0x01;
    private static final int FLAG_ENERGY_PRESENT = 0x08;
    private static final double CALORIES_PER_JOULE = 0.239005736;

    private boolean flip = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_heartrate, container, false);
    }

    @Override
    public void setValue(byte[] value) {
        LinearLayout measurement = (LinearLayout)findViewById(R.id.measurement);
        if (measurement == null) return;
        measurement.setVisibility(View.VISIBLE);

        int offset = 0;
        int flags = DataUtils.unsignedByteToInt(value[offset++]);

        Integer bpm = 0;
        if ((flags & FLAG_FORMAT_UINT16) != 0) {
            bpm = DataUtils.unsignedBytesToInt(value[offset], value[offset+1]);
            offset += 2;
        } else {
            bpm = DataUtils.unsignedByteToInt(value[offset++]);
        }

        TextView textRate = (TextView)findViewById(R.id.textRate);
        textRate.setText(bpm.toString());

        ImageView imgHeart = (ImageView)findViewById(R.id.imgHeart);
        imgHeart.setVisibility(View.VISIBLE);
        imgHeart.setImageResource(flip ? R.drawable.heart1 : R.drawable.heart2);
        flip = !flip;

        if ((flags & FLAG_ENERGY_PRESENT) != 0) {
            Integer energy = DataUtils.unsignedBytesToInt(value[offset], value[offset+1]);
            energy = (int)(energy * CALORIES_PER_JOULE);
            offset += 2;

            String calories = String.format("%d %s", energy, getString(R.string.calories));
            TextView textCalories = (TextView)findViewById(R.id.textCalories);
            textCalories.setVisibility(View.VISIBLE);
            textCalories.setText(calories);
        }
    }
}
