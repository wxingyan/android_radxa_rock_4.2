/******************************************************************************
 *
 *  Copyright (C) 2012 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/
package com.broadcom.app.leexplorer;

import android.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.broadcom.bt.gatt.BluetoothGattCharacteristic;

public abstract class ValueFragment extends Fragment {
    /**
     * Assign the data to be displayed
     * @param value The value to be displayed
     */
    public abstract void setValue(byte [] value);

    /**
     * Set whether the value is displayed read-only or accepts input.
     * @param readonly true, if value is read-only
     */
    public void setReadonly(boolean readonly) {
    }

    /**
     * Assign the user input value to a given characteristic
     * @param characteristic The characteristic to be updated
     * @return true, if the value was assigned correctly
     */
    public boolean assignValue(BluetoothGattCharacteristic characteristic) {
        return false;
    }

    /**
     * Find a view based on it's attribute ID
     * @param id Attribute ID to look for
     * @return View or null if no view with the given ID was found
     */
    public View findViewById(int id) {
        return getActivity().findViewById(id);
    }

    /**
     * Find a string array based on it's attribute ID
     * @param id Attribute ID to look for
     * @return String array or null, if no array was found
     */
    public String[] getStringArray(int id) {
        return getActivity().getResources().getStringArray(id);
    }

    /**
     * Assign a value to a TextView
     * @param id Resource id of the text view
     * @param text String to assign
     */
    public void setText(int id, String text) {
        TextView tv = (TextView)findViewById(id);
        if (tv != null) tv.setText(text);
    }

    /**
     * Set a given view to "Visible"
     * @param id Attribute id of the view element
     */
    public void setVisible(int id) {
        View view = findViewById(id);
        if (view != null) view.setVisibility(View.VISIBLE);
    }
}